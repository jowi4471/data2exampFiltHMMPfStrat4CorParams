# Data to Examples in Filter-based Portfolio Strategies in an HMM Setting with Varying Correlation Parametrizations

In this repository, you may find the data used in Section 5.3 of C. Erlwein-Sayer, S. Grimm, P. Ruckdeschel, J. Sass, and T. Sayer (2019): "Data to Examples in "Filter-based Portfolio Strategies in an HMM Setting with Varying Correlation Parametrizations" . 
The data is available as an R data frame with 2532 observations in 41 columns---one date column and daily closing quotes of 20 assets from the FTSE 100 index together with their respective log returns. The data set covers the ten year
time period between January 2008 and February 2018 and was obtained and downloaded from Yahoo finance April 1, 2018 through R-function tseries::get.hist.quote() .